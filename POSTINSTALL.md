On launch there are no users, and there is no user registration.

Log in for the first time with the following :
Username : shiori
Password : gopher

Then click the Settings gear icon and add an account.

Log out to use your new account.
The default account will then no longer work.
