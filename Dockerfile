# Use the Cloudron base image
FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

RUN mkdir -p /app/code /app/data

# Set the working directory
WORKDIR /app/code

# Install necessary dependencies and tools
RUN apt-get update && apt-get install -y \
    curl \
    unzip \
    && rm -rf /var/lib/apt/lists/*

# Fetch the latest release of Shiori from GitHub

RUN curl -L https://github.com/go-shiori/shiori/archive/refs/tags/v1.5.5.tar.gz | tar -zxvf - --strip-components 1 -C /app/code

RUN go build -ldflags '-s -w'

# Expose the necessary port
EXPOSE 8080

# Set the data directory for Shiori
ENV SHIORI_DIR /app/data

# Start Shiori
#CMD ["./shiori", "serve"]
ENTRYPOINT ["/app/code/shiori"]
CMD ["serve"]
